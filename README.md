Końcowy program zawiera się w folderach src i inc. 
W folderach tydzien_0 i tydzien_1 są programy zrobione
jedynie na potrzeby odpowiednio zerowego i pierwszego tygodnia.

Jak uruchomić program?

Gdy wejdziemy w terminalu w folder, w którym znajduje się nasz program,
wpisujemy "make". Tym poleceniem program zostanie skompilowany jak i zostanie
uruchomiony test "latwy."
Aby włączyć test trudny należy wpisać komendę:
./test_arytm_zesp  trudny


Uwaga! 
Projekt zawiera drugi branch o nazwie develop. W tym branchu moduł
LZespolona.cpp jest poszerzony o funkcję przeciążające +=,*= oraz ArgZesp, 
o które byliśmy proszeni na zajęciach 30.03.2020.

#include "LZespolona.hh"



void Wyswietl(LZespolona Skl1)
{
  cout<<"("<<Skl1.re<<showpos<<Skl1.im<<noshowpos<<"i)";
}


/*!
 * Realizuje dodanie dwoch liczb zespolonych.
 * Argumenty:
 *    Skl1 - pierwszy skladnik dodawania,
 *    Skl2 - drugi skladnik dodawania.
 * Zwraca:
 *    Sume dwoch skladnikow przekazanych jako parametry.
 */
LZespolona  operator + (LZespolona  Skl1,  LZespolona  Skl2)
{
  LZespolona  Wynik;

  Wynik.re = Skl1.re + Skl2.re;
  Wynik.im = Skl1.im + Skl2.im;
  return Wynik;
}

/* Funkcja odejmowania dla liczb zespolonych */

LZespolona operator - (LZespolona Skl1, LZespolona Skl2)
{
  LZespolona Wynik;

  Wynik.re = Skl1.re - Skl2.re;
  Wynik.im = Skl1.im - Skl2.im;
  
  return Wynik;  
}

/* Funkcja mnożenia dwóch liczb zespolonych. */


LZespolona operator * (LZespolona Skl1, LZespolona Skl2)
{
  LZespolona Wynik;

  Wynik.re=(Skl1.re*Skl2.re)-(Skl1.im*Skl2.im);
  Wynik.im=(Skl1.im*Skl2.re)+(Skl1.re*Skl2.im);

  return Wynik;
}

/* Funkcja dzielenia dwóch liczb Zespolonych. */

LZespolona operator / (LZespolona Skl1, LZespolona Skl2)
{
  LZespolona Wynik;

  Wynik=(Skl1*Sprzezenie(Skl2))/Modul2(Skl2);

  return Wynik;
}

/* Funkcja dzielenia liczby zespolonej przez liczbę typu double. */

LZespolona operator / (LZespolona Skl1,double Skl2)
{
  LZespolona Wynik;

  Wynik.re=Skl1.re/Skl2;
  Wynik.im=Skl1.im/Skl2;

  return Wynik;
}



/* Sprzężenie liczby Zespolonej. */

LZespolona Sprzezenie(LZespolona Skl1)
{
  LZespolona Wynik;
  
  Wynik.re=Skl1.re;
  Wynik.im=-Skl1.im;

  return Wynik;
}

/* Kwadrat modułu liczby zespolonej. */

double Modul2(LZespolona Skl1)
{

  return pow(Skl1.re,2)+pow(Skl1.im,2);
}












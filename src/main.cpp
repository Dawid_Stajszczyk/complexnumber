#include <iostream>
#include "BazaTestu.hh"
#include "LZespolona.hh"
#include "WyrazenieZesp.hh"
#include "Statystyki.hh"

using namespace std;




int main(int argc, char **argv)
{

  //////////////////////////////////////////////////////////////
   
   /* Testy.
   
  Testy są zakomentowane w celu prawidłowego działania quiz'u liczb zespolonych. */

/*
  cout<<"Testy zaimplementowanych funkcji."<<endl;

   LZespolona a={2,5};
   LZespolona b={3,2};

  cout<<"Wynik dodawania 2 liczb zespolonych: ";
   Wyswietl(a+b);
   cout<< endl;

  cout<<"Wynik odejmowania 2 liczb zespolonych: ";
   Wyswietl(a-b);
   cout<< endl;

  cout<<"Wynik mnożenia 2 liczb zespolonych: ";
   Wyswietl(a*b);
   cout<< endl;

  cout<<"Wynik dzielenia 2 liczb zespolonych: ";
   Wyswietl(a/b);
   cout<< endl;

   LZespolona ala, ala1;
   

  cout<<"Wprowadź liczbę zespoloną: ";
   cin>>ala;

   cout<<"Wprowadzona pierwsza liczba zespolona: "<<ala<<endl;

  cout<<"Wprowadź liczbę zespoloną: ";
   Wczytaj(ala1);

   cout<<"Wprowadzona druga liczba zespolona: "<<ala1<<endl;

   WyrazenieZesp ala2;


  cout<<"Wprowadź wyrażenie zespolone: ";
   Wczytaj(ala2);
    
  cout<<"Wprowadzone wyrażenie zespolone: ";
  cout<<ala2<<endl;

  cout<<"Wprowadzone wyrażenie zespolone (funkcja Wyswietl): ";
  Wyswietl(ala2);
  cout<<endl;

  cout<<"Wartość wprowadzone wyrażenia: ";
  cout<<Oblicz(ala2)<<endl;

  ////////////////////////////////////////////////////////////////
  */

  if (argc < 2) {
    cout << endl;
    cout << " Brak opcji okreslajacej rodzaj testu." << endl;
    cout << " Dopuszczalne nazwy to:  latwy, trudny." << endl;
    cout << endl;
    return 1;
  }


  BazaTestu   BazaT = { nullptr, 0, 0 };

  if (InicjalizujTest(&BazaT,argv[1]) == false) {
    cerr << " Inicjalizacja testu nie powiodla sie." << endl;
    return 1;
  }

LZespolona answer;
Statystyki quiz;
quiz.correct=0;
quiz.incorrect=0;

  
  cout << endl;
  cout << " Start testu arytmetyki zespolonej: " << argv[1] << endl;
  cout << endl;

  WyrazenieZesp   WyrZ_PytanieTestowe;
  
  while (PobierzNastpnePytanie(&BazaT,&WyrZ_PytanieTestowe)) {

    int licznik=0;

   
    
    cout<<"Podaj wynik operacji: ";
    cout<<WyrZ_PytanieTestowe<<" = "<<endl;

     cout<<"Twoja odpowiedź: ";

    do
    {
      cin.clear();
    
    cin>>answer;

    if(cin.fail())
    {
      cout<<"Blad zapisu liczby zespolonej. Sprobuj jeszcze raz."<<endl;
      licznik++;

    }
    cin.ignore(1024,'\n');
    } while (licznik<3 && cin.fail());

    if(Oblicz(WyrZ_PytanieTestowe)==answer)
    {
      cout<<"Odpowiedz poprawna"<<endl;
      quiz.correct++;
      

    }
    else
    {
      cout<<"Blad. Prawidlowym wynikiem jest: ";
      cout<<Oblicz(WyrZ_PytanieTestowe)<<endl;
      quiz.incorrect++;
      
    }
    
    

  }

  
  cout << endl;
  cout << " Koniec testu" << endl;
  cout << endl;

  quiz.procent=(quiz.correct*100.0)/(quiz.correct+quiz.incorrect);

  Wyswietl(quiz);

 

}

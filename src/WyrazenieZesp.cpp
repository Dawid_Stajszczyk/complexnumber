#include "WyrazenieZesp.hh"


/*
 * Tu nalezy zdefiniowac funkcje, ktorych zapowiedzi znajduja sie
 * w pliku naglowkowym.
 */

/* Funkcja wyświetlająca wyrażenie zespolona. */

void Wyswietl(WyrazenieZesp  WyrZ)
{
    Wyswietl(WyrZ.Arg1);
    
    switch (WyrZ.Op)
    {
    case Op_Dodaj:
        cout<<" + ";
        break;
    case Op_Odejmij:
        cout<<" - ";
        break;
    case Op_Mnoz:
        cout<<" * ";
        break;
    case Op_Dziel:
        cout<<" / ";
        break;
    
    }
    Wyswietl(WyrZ.Arg2);
}

/* Funkcja obliczająca wyrażenie zespolone. */

LZespolona Oblicz(WyrazenieZesp  WyrZ)
{
    LZespolona Wynik;

    switch(WyrZ.Op)
    {
        case Op_Dodaj:
        Wynik=WyrZ.Arg1+WyrZ.Arg2;
        return Wynik;

        case Op_Odejmij:
        Wynik=WyrZ.Arg1-WyrZ.Arg2;
        return Wynik;

        case Op_Mnoz:
        Wynik=WyrZ.Arg1*WyrZ.Arg2;
        return Wynik;

        case Op_Dziel:
        Wynik=WyrZ.Arg1/WyrZ.Arg2;
        return Wynik;

    }
    return WyrZ.Arg1;
}

/* Funkcja wczytująca wyrażenie zespolone: 
Argument jest przyjmowany przez referencję, ponieważ wczytana
liczba w funkcji Wczytaj musi być widoczna również poza nią. */


int Wczytaj(WyrazenieZesp &WyrZ)
{
    /* Gdy funkcja wczytująca liczbe zespoloną nie wczyta jej poprawnie,
    to zwróci "1", wtedy if się wykona i funkcja wczytująca wyrażenie zespolone,
    także zwróci "1"(bląd). */

    if(Wczytaj(WyrZ.Arg1))
    {
        return 1;
    }

    char sign;

    cin>>sign;

    switch(sign)
    {
        case '+':
        WyrZ.Op=Op_Dodaj;
        break;

        case '-':
        WyrZ.Op=Op_Odejmij;
        break;

        case '*':
        WyrZ.Op=Op_Mnoz;
        break;

        case '/':
        WyrZ.Op=Op_Dziel;
        break;

        default:
        {
            return 1;
        }
    }

    

    if(Wczytaj(WyrZ.Arg2))
    {
        return 1;
    }


    return 0;
}

/* Funkcja przeciążająca operator przesunięcia bitowego w lewo dla wyrażenie zespolonego.
Drugi argument (prawa strona) nie jest przyjmowany przez referencję, ponieważ funkcja ma jedynie
wyświetlić informację na ekran w jej wnętrzu, potem możeme 'zapomnieć' o otrzymanych danych. 
(Nie zmieniamy wartości żadnej zmiennej, a więc nie potrzebujemy widzień zmian dokonanych w funkcji poza nią.).

Funkcja zwraca typ ostream.

Jako argumenty przyjmuje typ ostream oraz typ WyrazenieZesp. Co to znaczy?
Wewnątrz funkcji nasza zmienną output będziemy się posługiwać jako cout'em na zewnątrz.
Druga zmienna jest zmienna, którą należy wypisać na ekran.

W tej funkcji nie przewidujemy usterek typu: źle wczytane wyrażenie zespolone do zmiennej WyrZ
ponieważ o to dba funkcja Wczytaj. */

ostream & operator <<(ostream &output, WyrazenieZesp WyrZ)
{
    output<<WyrZ.Arg1;

    switch(WyrZ.Op)
    {
        case Op_Dodaj:
        output<<"+";
        break;

        case Op_Odejmij:
        output<<"-";
        break;

        case Op_Mnoz:
        output<<"*";
        break;

        case Op_Dziel:
        output<<"/";
        break;
    }

    output<<WyrZ.Arg2;

    return output;
}
#include "LZespolona.hh"


/* Funkcja wyświetlająca liczbę zespoloną. */

void Wyswietl(LZespolona Skl1)
{
  cout<<"("<<Skl1.re<<showpos<<Skl1.im<<noshowpos<<"i)";
}


/*!
 * Realizuje dodanie dwoch liczb zespolonych.
 * Argumenty:
 *    Skl1 - pierwszy skladnik dodawania,
 *    Skl2 - drugi skladnik dodawania.
 * Zwraca:
 *    Sume dwoch skladnikow przekazanych jako parametry.
 */
LZespolona  operator + (LZespolona  Skl1,  LZespolona  Skl2)
{
  LZespolona  Wynik;

  Wynik.re = Skl1.re + Skl2.re;
  Wynik.im = Skl1.im + Skl2.im;
  return Wynik;
}

/* Funkcja odejmowania dla liczb zespolonych */

LZespolona operator - (LZespolona Skl1, LZespolona Skl2)
{
  LZespolona Wynik;

  Wynik.re = Skl1.re - Skl2.re;
  Wynik.im = Skl1.im - Skl2.im;
  
  return Wynik;  
}

/* Funkcja mnożenia dwóch liczb zespolonych. */


LZespolona operator * (LZespolona Skl1, LZespolona Skl2)
{
  LZespolona Wynik;

  Wynik.re=(Skl1.re*Skl2.re)-(Skl1.im*Skl2.im);
  Wynik.im=(Skl1.im*Skl2.re)+(Skl1.re*Skl2.im);

  return Wynik;
}

/* Funkcja dzielenia dwóch liczb Zespolonych. */

LZespolona operator / (LZespolona Skl1, LZespolona Skl2)
{
  LZespolona Wynik;

  Wynik=(Skl1*Sprzezenie(Skl2))/Modul2(Skl2);

  return Wynik;
}

/* Funkcja dzielenia liczby zespolonej przez liczbę typu double. */

LZespolona operator / (LZespolona Skl1,double Skl2)
{
  LZespolona Wynik;

  Wynik.re=Skl1.re/Skl2;
  Wynik.im=Skl1.im/Skl2;

  return Wynik;
}



/* Sprzężenie liczby Zespolonej. */

LZespolona Sprzezenie(LZespolona Skl1)
{
  LZespolona Wynik;
  
  Wynik.re=Skl1.re;
  Wynik.im=-Skl1.im;

  return Wynik;
}

/* Kwadrat modułu liczby zespolonej. */

double Modul2(LZespolona Skl1)
{

  return pow(Skl1.re,2)+pow(Skl1.im,2);
}

/* Przeciążenie operatora porównania dla liczb zespolonych.
Uznajemy, że liczby sa równe gdy ich częsci rzeczywiste i urojone
różnia się od siebie o mnie niż 0.1                          */

bool operator == (LZespolona Skl1, LZespolona Skl2)
{
  if(abs(Skl1.re-Skl2.re)<0.1 && abs(Skl1.im-Skl2.im)<0.1)
  {
    return true;
  }
  else
  {
    return false;
  }
  
}

/* Funkcja wczytująca liczbę zespoloną.

Argument jest przyjmowany przez referencje, aby
poza funkcją postać wczytanej liczby była zachowana. */

int Wczytaj(LZespolona &Skl1)
{
  char sign;
  cin>>sign;

  if(sign!='(')
  {
    return 1;
  }

  cin>>Skl1.re;
   
   /* Jeśli odczytanie wartości nie powiodło się (Zostało wprowadzone coś innego od liczby rzeczywistej)
   to funkcja cin.fail() zwróci true a więc funkcja się zakończy niepowodzeniem. Jednak przed tym
   koniecznie jest użycie funkcji cin.clear(), która usunie ustawione flagi błędu.        */

   if(cin.fail())
  {
      cin.clear(); 
    return 1;
  }

  cin>>Skl1.im;
 
  if(cin.fail())
  {
      cin.clear(); 
    return 1;
  }

/* Tutaj nie używam metody cin.clear(), ponieważ cin zawsze odczyta
poprawnie znak i flaga błędu failbit ani badbit nie ustawi się.      */
  cin>>sign;
  if(sign!='i')
  {
    return 1;
  }

  cin>>sign;
  if(sign!=')')
  {
    return 1;
  }

  

  return 0;
}




/* Funkcja przeciążająca operator przesunięcia bitowego w prawo dla liczby zespolonej.

Funkcja zwraca typ istream.

Funkcja przyjmuje 2 argumenty: zmienną typu istream oraz zmienną typu LZespolona.

Drugi argument jest przyjmowany przez referecję, ponieważ istotne jest to aby wartości
wczytane do zmiennej Skl1 byly widoczne poza funkcją. 

Zmiennej input wewnątrz funkcji używamy jak cin'a poza nią. */




istream & operator >>(istream &input, LZespolona &Skl1)
{
  char sign;

  input>>sign;

/* Liczba zespolona jest postaci (re+imi). Należy zatem zauważyć, że 
flaga błędu strumienia failbit powinna zostać ustawiona, gdy wprowadzony znak jest
różny od '('. */

  if(sign!='(')
  {
    input.setstate(ios_base::failbit);
    return input;
  }

  input>>Skl1.re;

  /* W tym wypadku flaga błedu ustawi się sama w razie nieprawidłowego wczytania double'a */

  if(input.fail())
  {
    return input;
  }

  input>>Skl1.im;

   if(input.fail())
  {
    return input;
  }

  input>>sign;

  if(sign!='i')
  {
    input.setstate(ios_base::failbit);
    return input;
  }

  input>>sign;

  if(sign!=')')
  {
     input.setstate(ios_base::failbit);
    return input;
  }

  return input;
}



/* Funkcja przeciążająca operator przesunięcia bitowego w lewo dla liczby zespolonej.

showpos-Wypisuje liczbę wraz ze znakiem.
Po użyciu showpos, należy wyłączyć jego działanie: polecenie noshowpos. */



ostream & operator << (ostream &output, LZespolona Skl1)
{
  output<<"("<<Skl1.re<<showpos<<Skl1.im<<noshowpos<<"i)";

  return output;
}


 



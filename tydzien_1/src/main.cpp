#include <iostream>
#include "BazaTestu.hh"
#include "LZespolona.hh"
#include "WyrazenieZesp.hh"

using namespace std;




int main(int argc, char **argv)
{

  //////////////////////////////////////////////////////////////
   
   /* Testy. */

  cout<<"Testy zaimplementowanych funkcji."<<endl;

   LZespolona a={2,5};
   LZespolona b={3,2};

  cout<<"Wynik dodawania 2 liczb zespolonych: ";
   Wyswietl(a+b);
   cout<< endl;

  cout<<"Wynik odejmowania 2 liczb zespolonych: ";
   Wyswietl(a-b);
   cout<< endl;

  cout<<"Wynik mnożenia 2 liczb zespolonych: ";
   Wyswietl(a*b);
   cout<< endl;

  cout<<"Wynik dzielenia 2 liczb zespolonych: ";
   Wyswietl(a/b);
   cout<< endl;

   LZespolona ala, ala1;
   

  cout<<"Wprowadź liczbę zespoloną: ";
   cin>>ala;

   cout<<"Wprowadzona pierwsza liczba zespolona: "<<ala<<endl;

  cout<<"Wprowadź liczbę zespoloną: ";
   Wczytaj(ala1);

   cout<<"Wprowadzona druga liczba zespolona: "<<ala1<<endl;

   WyrazenieZesp ala2;


  cout<<"Wprowadź wyrażenie zespolone: ";
   Wczytaj(ala2);
    
  cout<<"Wprowadzone wyrażenie zespolone: ";
  cout<<ala2<<endl;

  cout<<"Wprowadzone wyrażenie zespolone (funkcja Wyswietl): ";
  Wyswietl(ala2);
  cout<<endl;

  cout<<"Wartość wprowadzone wyrażenia: ";
  cout<<Oblicz(ala2)<<endl;





   
   




   







  ////////////////////////////////////////////////////////////////

  if (argc < 2) {
    cout << endl;
    cout << " Brak opcji okreslajacej rodzaj testu." << endl;
    cout << " Dopuszczalne nazwy to:  latwy, trudny." << endl;
    cout << endl;
    return 1;
  }


  BazaTestu   BazaT = { nullptr, 0, 0 };

  if (InicjalizujTest(&BazaT,argv[1]) == false) {
    cerr << " Inicjalizacja testu nie powiodla sie." << endl;
    return 1;
  }


  
  cout << endl;
  cout << " Start testu arytmetyki zespolonej: " << argv[1] << endl;
  cout << endl;

  WyrazenieZesp   WyrZ_PytanieTestowe;
  
  while (PobierzNastpnePytanie(&BazaT,&WyrZ_PytanieTestowe)) {
    cout << " Czesc rzeczywista pierwszego argumentu: ";
    cout << WyrZ_PytanieTestowe.Arg1.re << endl;
  }

  
  cout << endl;
  cout << " Koniec testu" << endl;
  cout << endl;

 

}
